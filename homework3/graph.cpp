//
// Created by littlebear on 2022/5/14.
//

#include <iostream>
#include <vector>
#include <queue>

#define INF 0x3f3f3f3f

using namespace std;

int n, m; // n: 顶点数， m: 边数
vector<bool> vis;
vector<vector<int>> graph;

void dfs(int u) {
    if (vis[u]) return;
    printf("%d ", u);
    vis[u] = true;
    for (int i = 0; i < graph[u].size(); i++) {
        int v = graph[u][i];
        if (!vis[v]) {
            dfs(v);
        }
    }
}

void bfs(int u) {
    printf("%d ", u);
    vis[u] = true;
    queue<int> q;
    q.push(u);
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        for (int i = 0; i < graph[u].size(); i++) {
            int v = graph[u][i];
            if (!vis[v]) {
                printf("%d ", v);
                vis[v] = true;
                q.push(v);
            }
        }
    }
}


int main() {
    cin >> n >> m;
    vis.resize(n + 1, false);
    graph.resize(n + 1);

    for (int i = 1; i <= m; i++) {
        int u, v;
        cin >> u >> v;
        graph[u].push_back(v);
    }

    dfs(1);
    printf("\n");

    vis.assign(n + 1, false);
    bfs(1);
    printf("\n");

    return 0;
}