Compare different kinds of sorting algorithms and evaluate their efficiency.

- Bubble sort
- Insertion sort
- Merge sort
- Quick sort

Results:
```text
Array size: 10
Bubble sort: 0.002 ms
Insertion sort: 0.001 ms
Merge sort: 0.001 ms
Quick sort: 0.001 ms

Array size: 100
Bubble sort: 0.026 ms
Insertion sort: 0.017 ms
Merge sort: 0.012 ms
Quick sort: 0.006 ms

Array size: 1000
Bubble sort: 2.367 ms
Insertion sort: 1.602 ms
Merge sort: 0.149 ms
Quick sort: 0.076 ms

Array size: 10000
Bubble sort: 290.153 ms
Insertion sort: 166.927 ms
Merge sort: 1.668 ms
Quick sort: 1.023 ms

Array size: 100000
Bubble sort: 32745.190 ms
Insertion sort: 17152.222 ms
Merge sort: 19.322 ms
Quick sort: 12.595 ms

Array size: 1000000
Bubble sort: Too long time...
Insertion sort: Too long time...
Merge sort: 215.802 ms
Quick sort: 167.150 ms
```


Source code in folder `sort.cpp`.