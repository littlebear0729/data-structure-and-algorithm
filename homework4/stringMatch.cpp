//
// Created by littlebear on 2022/5/24.
//

#include <iostream>
#include <cstring>

#define MAXN 1005

using namespace std;

int sunday(char *str, char *pattern) {
    int n = strlen(str), m = strlen(pattern);
    int shift[130]; // shift[i]表示i字符距离最后的长度，即需要往后移动的距离
    int i, j;
    for (i = 0; i <= 128; i++) {
        shift[i] = m + 1;
    }
    for (i = 0; i < m; i++) {
        shift[pattern[i]] = m - i;
    }
//    for (i = 0; i <= 128; i++) {
//        cout << shift[i] << " ";
//    }
//    cout << endl;
    int s = 0;
    while (s <= n - m) {
        j = 0;
        while (str[s + j] == pattern[j]) {
            j++;
            // 匹配成功
            if (j >= m) {
                return s;
            }
        }
        // 后移
        s += shift[str[s + m]];
    }
    return -1;
}

int kmp(char *str, char *pattern) {
    int n = strlen(str), m = strlen(pattern);
    int i, j;
    int next[m];
    // 计算next函数
    next[0] = -1;
    for (i = 1; i < m; i++) {
        j = next[i - 1];
        while (j != -1 && pattern[j + 1] != pattern[i]) {
            j = next[j];
        }
        if (pattern[j + 1] == pattern[i]) {
            next[i] = j + 1;
        } else {
            next[i] = -1;
        }
    }
//    for (i = 0; i < m; i++) {
//        cout << next[i] << " ";
//    }
//    cout << endl;
    int s = 0;
    while (s <= n - m) {
        j = 0;
        while (str[s + j] == pattern[j]) {
            j++;
            // 匹配成功
            if (j >= m) {
                return s;
            }
        }
        // 后移
        if (next[j] == -1) {
            s += 1;
        } else {
            s += next[j];
        }
    }
    return -1;
}

int main() {
    char str[MAXN], pattern[MAXN];
//    cin >> str;
//    cin >> pattern;
    cin.getline(str, MAXN);
    cin.getline(pattern, MAXN);
    int index = sunday(str, pattern);
    if (index == -1) {
        cout << "Not Found" << endl;
    } else {
        cout << index << endl;
    }

    index = kmp(str, pattern);
    if (index == -1) {
        cout << "Not Found" << endl;
    } else {
        cout << index << endl;
    }
    return 0;
}